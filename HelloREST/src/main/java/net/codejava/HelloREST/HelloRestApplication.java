package net.codejava.HelloREST;

import java.util.Scanner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import net.codejava.HelloREST.hello.MyService;

@SpringBootApplication
public class HelloRestApplication {
	

	public static void main(String[] args) {
		SpringApplication.run(HelloRestApplication.class, args);

	}

}
