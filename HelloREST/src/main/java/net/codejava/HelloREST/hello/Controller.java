package net.codejava.HelloREST.hello;


import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;




@RestController
public class Controller {
	@Autowired
	private MyService service;
	
	@RequestMapping(value="/dataset/stats/total")
	public Total totalEvents() {
		return this.service.getNoEvents();
	}
	
	@RequestMapping(value="/dataset/stats/offenses")
	public Map<String, Long> totalOffenses() {
		return this.service.getOffenses();
	}
	
	@RequestMapping(method = RequestMethod.DELETE,value="/dataset/{id}")
	public boolean deleteEvent(@PathVariable String id){
		return this.service.deleteEvent(id);
	}
	
	@RequestMapping(method = RequestMethod.POST, value="/dataset")
	public void addEvent(@RequestBody Event event) {
		this.service.addEvent(event);
	}
}
