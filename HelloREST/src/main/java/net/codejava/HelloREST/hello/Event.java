package net.codejava.HelloREST.hello;

public class Event {

	private String cmplnt_num;

	private String ky_cd;

	public void setCMPLNT_NUM(String CMPLNT_NUM) {
		this.cmplnt_num = CMPLNT_NUM;
	}


	public void setKY_CD(String KY_CD) {
		this.ky_cd = KY_CD;
	}


	public String getCMPLNT_NUM() {
		return this.cmplnt_num;
	}


	public String getKY_CD() {
		return this.ky_cd;
	}


	public Event(String CMPLNT_NUM, String KY_CD) {
		this.cmplnt_num = CMPLNT_NUM;
		this.ky_cd = KY_CD;
	}

	public Event() {
		// TODO Auto-generated constructor stub
	}

}
