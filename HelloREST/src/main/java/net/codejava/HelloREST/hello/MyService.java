package net.codejava.HelloREST.hello;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.tomcat.util.file.ConfigurationSource.Resource;
import org.springframework.stereotype.Service;

@Service
public class MyService {

	private String PATH = "D:\\retina\\HelloREST\\NYPD_Complaint_Data_Historic.csv";

	public Total getNoEvents() {
		String line = "";
		int noEv = 0;
		try {
			BufferedReader br = new BufferedReader(new FileReader(PATH));
			while ((line = br.readLine()) != null) {
//				if(!line.equals("null"))
//				{
					noEv++;
				//}
			}

			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Total total = new Total();
		total.setNoEvents(noEv - 1);// it's -1 because it reads also the header of the file

		return total;
	}

	public boolean deleteEvent(String id) {
		boolean success = false;

		String line;
		String[] arr = null;
		try {
			BufferedReader br = new BufferedReader(new FileReader(PATH));
			File updated=new File("updated.csv");
			if(!updated.exists()) {
				updated.createNewFile();
			}
			BufferedWriter writer = new BufferedWriter(new FileWriter(updated));
			while ((line = br.readLine()) != null) {
				if(line.contains(id)){
					success=true;
					continue;
				}
				writer.write(line+"\n");
			}
			//writer.write(line+"\n");

			writer.close();
			br.close();
		} catch (Exception e) {

			e.printStackTrace();
		}

		return success;
	}


	public void addEvent(Event event) {
		String line;
		String[] arr = null;
		try {
			BufferedReader br = new BufferedReader(new FileReader(PATH));
			BufferedWriter writer = new BufferedWriter(new FileWriter(PATH, true));
			while ((line = br.readLine()) != null) {
				continue;
			}

			writer.write(event.getCMPLNT_NUM() + "," + null + "," + null + "," + null + "," + null + "," + null + ","
					+ null + "," + event.getKY_CD() + "," + null + "," + null + "," + null + "," + null + "," + null
					+ "," + null + "," + null + "," + null + "," + null + "," + null + "," + null + "," + null + ","
					+ null + "," + null + "," + null + "," + null + "\n");
			writer.close();
			br.close();
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	public List<String> getList() {
		String line = "";
		String splitBy = ",";
		List<String> offenses = new ArrayList<String>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(PATH));
			while ((line = br.readLine()) != null) {
				//if(!line.equals("null")){
					String[] array = line.split(splitBy);
					offenses.add(array[7]);
				//}
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return offenses;
	}

	public Map<String, Long> getOffenses() {
		List<String> offenses = getList();
		for (int i = 0; i < offenses.size(); i++) {
			if (offenses.get(i).equals("KY_CD"))// it removes the header of the column
				offenses.remove(i);
		}
		Map<String, Long> count = offenses.stream()
				.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
		//<=> select ky_cd, count(ky_cd)
		//from nypd_complaint_data_historic
		//group by ky_cd;
		
		return count;
	}

}
